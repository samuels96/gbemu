#pragma once

#include "input.h"
#include "interrupts.h"
#include "cpu.h"
#include "main.h"

struct Joypad Joypad;

void InputInit()
{
	SDL_SetEventFilter(keyInputEventFilter, NULL);

	Joypad.buttons = 0xFF;
	Joypad.directions = 0xFF;
	Joypad.select = INPUT_SELECT_BUTTONS;
}


int keyInputEventFilter(void* null, SDL_Event* event)
{
	switch (event->type) {
	case SDL_KEYDOWN:
		switch (event->key.keysym.sym) {
		case SDLK_RETURN:
			Joypad.buttons &= ~INPUT_BUTTONS_START;
			IR.fl |= IR_JOYPAD;
			CPU.halt = 0;
			break;
		case SDLK_TAB:
			Joypad.buttons &= ~INPUT_BUTTONS_SELECT;
			IR.fl |= IR_JOYPAD;
			CPU.halt = 0;
			break;
		case SDLK_s:
			Joypad.buttons &= ~INPUT_BUTTONS_A;
			IR.fl |= IR_JOYPAD;
			CPU.halt = 0;
			break;
		case SDLK_a:
			Joypad.buttons &= ~INPUT_BUTTONS_B;
			IR.fl |= IR_JOYPAD;
			CPU.halt = 0;
			break;
		case SDLK_UP:
			Joypad.directions &= ~INPUT_DIRECTIONS_UP;
			IR.fl |= IR_JOYPAD;
			CPU.halt = 0;
			break;
		case SDLK_DOWN:
			Joypad.directions &= ~INPUT_DIRECTIONS_DOWN;
			IR.fl |= IR_JOYPAD;
			CPU.halt = 0;
			break;
		case SDLK_LEFT:
			Joypad.directions &= ~INPUT_DIRECTIONS_LEFT;
			IR.fl |= IR_JOYPAD;
			CPU.halt = 0;
			break;
		case SDLK_RIGHT:
			Joypad.directions &= ~INPUT_DIRECTIONS_RIGHT;
			IR.fl |= IR_JOYPAD;
			CPU.halt = 0;
			break;
		default:
			break;
		}
		break;
	case SDL_KEYUP:
		switch (event->key.keysym.sym) {
		case SDLK_RETURN:
			Joypad.buttons |= INPUT_BUTTONS_START;
			IR.fl |= IR_JOYPAD;
			CPU.halt = 0;
			break;
		case SDLK_TAB:
			Joypad.buttons |= INPUT_BUTTONS_SELECT;
			IR.fl |= IR_JOYPAD;
			CPU.halt = 0;
			break;
		case SDLK_s:
			Joypad.buttons |= INPUT_BUTTONS_A;
			IR.fl |= IR_JOYPAD;
			CPU.halt = 0;
			break;
		case SDLK_a:
			Joypad.buttons |= INPUT_BUTTONS_B;
			IR.fl |= IR_JOYPAD;
			CPU.halt = 0;
			break;
		case SDLK_UP:
			Joypad.directions |= INPUT_DIRECTIONS_UP;
			IR.fl |= IR_JOYPAD;
			CPU.halt = 0;
			break;
		case SDLK_DOWN:
			Joypad.directions |= INPUT_DIRECTIONS_DOWN;
			IR.fl |= IR_JOYPAD;
			CPU.halt = 0;
			break;
		case SDLK_LEFT:
			Joypad.directions |= INPUT_DIRECTIONS_LEFT;
			IR.fl |= IR_JOYPAD;
			CPU.halt = 0;
			break;
		case SDLK_RIGHT:
			Joypad.directions |= INPUT_DIRECTIONS_RIGHT;
			IR.fl |= IR_JOYPAD;
			CPU.halt = 0;
			break;
		default:
			break;
		}
		break;
	}

	return 0;
}


void inputHandle()
{
	SDL_PumpEvents();
}
