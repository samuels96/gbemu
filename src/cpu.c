#include "cpu.h"
#include "cb.h"
#include "interrupts.h"
#include "memory.h"
#include "gpu.h"
#include "input.h"
#include <Windows.h>
#include <stdio.h>

void (*CPUOperation[256])(void) = {
    /*0x0*/ NOP, LD_BC_IM16, LD_BCP_A, INC_BC, INC_B, DEC_B, LD_B_IM8, RLCA,
    /*0x8*/ LD_IM16P_SP, ADD_HL_BC, LD_A_BCP, DEC_BC, INC_C, DEC_C, LD_C_IM8, RRCA, 
    /*0x10*/ STOP, LD_DE_IM16, LD_DEP_A, INC_DE, INC_D, DEC_D, LD_D_IM8, RLA,
    /*0x18*/ JR_IM8, ADD_HL_DE, LD_A_DEP, DEC_DE, INC_E, DEC_E, LD_E_IM8, RRA,
    /*0x20*/ JR_NZ_IM8, LD_HL_IM16, LDI_HLP_A, INC_HL, INC_H, DEC_H, LD_H_IM8, DAA,
    /*0x28*/ JR_Z_IM8, ADD_HL_HL, LDI_A_HLP, DEC_HL, INC_L, DEC_L, LD_L_IM8, CPL,
    /*0x30*/ JR_NC_IM8, LD_SP_IM16, LDD_HLP_A, INC_SP, INC_HLP, DEC_HLP, LD_HLP_IM8, SCF,
    /*0x38*/ JR_C_IM8, ADD_HL_SP, LDD_A_HLP, DEC_SP, INC_A, DEC_A, LD_A_IM8, CCF,
    /*0x40*/ LD_B_B, LD_B_C, LD_B_D, LD_B_E, LD_B_H, LD_B_L, LD_B_HLP, LD_B_A,
    /*0x48*/ LD_C_B, LD_C_C, LD_C_D, LD_C_E, LD_C_H, LD_C_L, LD_C_HLP, LD_C_A,
    /*0x50*/ LD_D_B, LD_D_C, LD_D_D, LD_D_E, LD_D_H, LD_D_L, LD_D_HLP, LD_D_A,
    /*0x58*/ LD_E_B, LD_E_C, LD_E_D, LD_E_E, LD_E_H, LD_E_L, LD_E_HLP, LD_E_A,
    /*0x60*/ LD_H_B, LD_H_C, LD_H_D, LD_H_E, LD_H_H, LD_H_L, LD_H_HLP, LD_H_A,
    /*0x68*/ LD_L_B, LD_L_C, LD_L_D, LD_L_E, LD_L_H, LD_L_L, LD_L_HLP, LD_L_A,
    /*0x70*/ LD_HLP_B, LD_HLP_C, LD_HLP_D, LD_HLP_E, LD_HLP_H, LD_HLP_L, HALT, LD_HLP_A,
	/*0x78*/ LD_A_B, LD_A_C, LD_A_D, LD_A_E, LD_A_H, LD_A_L, LD_A_HLP, LD_A_A,
    /*0x80*/ ADD_A_B, ADD_A_C, ADD_A_D, ADD_A_E, ADD_A_H, ADD_A_L, ADD_A_HLP, ADD_A_A,
    /*0x88*/ ADC_A_B, ADC_A_C, ADC_A_D, ADC_A_E, ADC_A_H, ADC_A_L, ADC_A_HLP, ADC_A_A,
    /*0x90*/ SUB_A_B, SUB_A_C, SUB_A_D, SUB_A_E, SUB_A_H, SUB_A_L, SUB_A_HLP, SUB_A_A,
    /*0x98*/ SBC_A_B, SBC_A_C, SBC_A_D, SBC_A_E, SBC_A_H, SBC_A_L, SBC_A_HLP, SBC_A_A,
    /*0xa0*/ AND_A_B, AND_A_C, AND_A_D, AND_A_E, AND_A_H, AND_A_L, AND_A_HLP, AND_A_A,
    /*0xa8*/ XOR_A_B, XOR_A_C, XOR_A_D, XOR_A_E, XOR_A_H, XOR_A_L, XOR_A_HLP, XOR_A_A,
    /*0xb0*/ OR_A_B, OR_A_C, OR_A_D, OR_A_E, OR_A_H, OR_A_L, OR_A_HLP, OR_A_A,
    /*0xb8*/ CP_A_B, CP_A_C, CP_A_D, CP_A_E, CP_A_H, CP_A_L, CP_A_HLP, CP_A_A,
    /*0xc0*/ RET_NZ, POP_BC, JP_NZ_IM16, JP_IM16, CALL_NZ_IM16, PUSH_BC, ADD_A_IM8, RST_0,
    /*0xc8*/ RET_Z, RET, JP_Z_IM16, CB_IM8, CALL_Z_IM16, CALL_IM16, ADC_A_IM8, RST_8,
    /*0xd0*/ RET_NC, POP_DE, JP_NC_IM16, UNDEF, CALL_NC_IM16, PUSH_DE, SUB_A_IM8, RST_10,
    /*0xd8*/ RET_C, RETI, JP_C_IM16, UNDEF, CALL_C_IM16, UNDEF, SBC_A_IM8, RST_18,
    /*0xe0*/ LD_FF_IM8_A, POP_HL, LD_FF_C_A, UNDEF, UNDEF, PUSH_HL, AND_IM8, RST_20,
    /*0xe8*/ ADD_SP_IM8, JP_HL, LD_IM16P_A, UNDEF, UNDEF, UNDEF, XOR_IM8, RST_28,
    /*0xf0*/ LD_A_FF_IM8, POP_AF, LD_A_FF_C, DI, UNDEF, PUSH_AF, OR_IM8, RST_30,
    /*0xf8*/ LD_HL_SP_IM8, LD_SP_HL, LD_A_IM16P, EI, UNDEF, UNDEF, CP_IM8, RST_38
};

struct CPUInstance CPU;

void bpoint() {
	char str[100];
	int i;
	scanf_s("%s %d", str, &i);
}

void CPUInit(void) {
	REG_AF = 0x01B0;
	REG_BC = 0x0013;
	REG_DE = 0x00D8;
	REG_HL = 0x014D;
	CPU.SP = 0xFFFE;
	CPU.PC = 0x100;
}

void dbug() {
	printf("\nSCANLINE: %d", GPU.scanline);
	printf("\n\nPC:%04x SP:%04x ", CPU.PC, CPU.SP);
	printf("\nAF: %04x\nBC: %04x\nDE: %04x\nHL: %04x\nFLAGS Z:%d N:%d H:%d C:%d ",
		REG_AF, REG_BC, REG_DE, REG_HL, FLAG_Z, FLAG_N, FLAG_H, FLAG_C);
	printf("\nIME: %04x | IE: %04x | IF: %04x", IR.masterEnable, IR.enable, IR.fl);
	printf("\n| OPERATION: %04x", readByte(CPU.PC));
	//bpoint();
}

void CPUStep(void) {

	CPUOperation[readByte(CPU.PC)]();
}

//////////////////////////
//8-BIT LOAD INSTRUCTIONS
// LD nn, n
static inline void LD_B_IM8(void){
	uint8_t im8 = readByte(CPU.PC + 1);

	REG_B = im8;
	CPU.cycles = 8;
	CPU.PC += 2;
}

static inline void LD_C_IM8(void){
	uint8_t im8 = readByte(CPU.PC + 1);

	REG_C = im8;
	CPU.cycles = 8;
	CPU.PC += 2;
}

static inline void LD_D_IM8(void){
	uint8_t im8 = readByte(CPU.PC + 1);

	REG_D = im8;
	CPU.cycles = 8;
	CPU.PC += 2;
}

static inline void LD_E_IM8(void){
	uint8_t im8 = readByte(CPU.PC + 1);

	REG_E = im8;
	CPU.cycles = 8;
	CPU.PC += 2;
}

static inline void LD_H_IM8(void){
	uint8_t im8 = readByte(CPU.PC + 1);

	REG_H = im8;
	CPU.cycles = 8;
	CPU.PC += 2;
}

static inline void LD_L_IM8(void){
	uint8_t im8 = readByte(CPU.PC + 1);

	REG_L = im8;
	CPU.cycles = 8;
	CPU.PC += 2;
}
//end of LD nn, n


static inline void LD_B_A(void) {
	REG_B = REG_A;
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void LD_B_B(void) {
	REG_B = REG_B;
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void LD_B_C(void) {
	REG_B = REG_C;
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void LD_B_D(void) {
	REG_B = REG_D;
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void LD_B_E(void) {
	REG_B = REG_E;
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void LD_B_H(void) {
	REG_B = REG_H;
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void LD_B_L(void) {
	REG_B = REG_L;
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void LD_B_HLP(void) {
	REG_B = readByte(REG_HL);
	CPU.cycles = 8;
	CPU.PC++;
}

static inline void LD_C_A(void) {
	REG_C = REG_A;
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void LD_C_B(void) {
	REG_C = REG_B;
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void LD_C_C(void) {
	REG_C = REG_C;
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void LD_C_D(void) {
	REG_C = REG_D;
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void LD_C_E(void) {
	REG_C = REG_E;
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void LD_C_H(void) {
	REG_C = REG_H;
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void LD_C_L(void) {
	REG_C = REG_L;
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void LD_C_HLP(void) {
	REG_C = readByte(REG_HL);
	CPU.cycles = 8;
	CPU.PC++;
}

static inline void LD_D_A(void) {
	REG_D = REG_A;
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void LD_D_B(void) {
	REG_D = REG_B;
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void LD_D_C(void) {
	REG_D = REG_C;
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void LD_D_D(void) {
	REG_D = REG_D;
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void LD_D_E(void) {
	REG_D = REG_E;
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void LD_D_H(void) {
	REG_D = REG_H;
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void LD_D_L(void) {
	REG_D = REG_L;
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void LD_D_HLP(void) {
	REG_D = readByte(REG_HL);
	CPU.cycles = 8;
	CPU.PC++;
}

static inline void LD_E_A(void) {
	REG_E = REG_A;
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void LD_E_B(void) {
	REG_E = REG_B;
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void LD_E_C(void) {
	REG_E = REG_C;
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void LD_E_D(void) {
	REG_E = REG_D;
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void LD_E_E(void) {
	REG_E = REG_E;
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void LD_E_H(void) {
	REG_E = REG_H;
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void LD_E_L(void) {
	REG_E = REG_L;
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void LD_E_HLP(void) {
	REG_E = readByte(REG_HL);
	CPU.cycles = 8;
	CPU.PC++;
}

static inline void LD_H_B(void) {
	REG_H = REG_B;
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void LD_H_C(void) {
	REG_H = REG_C;
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void LD_H_D(void) {
	REG_H = REG_D;
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void LD_H_E(void) {
	REG_H = REG_E;
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void LD_H_H(void) {
	REG_H = REG_H;
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void LD_H_L(void) {
	REG_H = REG_L;
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void LD_H_HLP(void) {
	REG_H = readByte(REG_HL);
	CPU.cycles = 8;
	CPU.PC++;
}

static inline void LD_H_A(void) {
	REG_H = REG_A;
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void LD_L_B(void) {
	REG_L = REG_B;
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void LD_L_C(void) {
	REG_L = REG_C;
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void LD_L_D(void) {
	REG_L = REG_D;
	CPU.cycles = 4;
	CPU.PC++;

}

static inline void LD_L_E(void) {
	REG_L = REG_E;
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void LD_L_H(void) {
	REG_L = REG_H;
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void LD_L_L(void) {
	REG_L = REG_L;
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void LD_L_HLP(void) {
	REG_L = readByte(REG_HL);
	CPU.cycles = 8;
	CPU.PC++;
}

static inline void LD_L_A(void) {
	REG_L = REG_A;
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void LD_HLP_B(void) {
	writeByte(REG_HL, REG_B);
	CPU.cycles = 8;
	CPU.PC++;
}

static inline void LD_HLP_C(void) {
	writeByte(REG_HL, REG_C);
	CPU.cycles = 8;
	CPU.PC++;
}

static inline void LD_HLP_D(void) {
	writeByte(REG_HL, REG_D);
	CPU.cycles = 8;
	CPU.PC++;
}

static inline void LD_HLP_E(void) {
	writeByte(REG_HL, REG_E);
	CPU.cycles = 8;
	CPU.PC++;
}

static inline void LD_HLP_H(void) {
	writeByte(REG_HL, REG_H);
	CPU.cycles = 8;
	CPU.PC++;
}

static inline void LD_HLP_L(void) {
	writeByte(REG_HL, REG_L);
	CPU.cycles = 8;
	CPU.PC++;
}

static inline void LD_HLP_IM8(void) {
	uint8_t im8 = readByte(CPU.PC + 1);

	writeByte(REG_HL, im8);
	CPU.cycles = 12;
	CPU.PC += 2;
}
//end of LD r1, r2

//LD A,n
static inline void LD_A_A(void) {
	REG_A = REG_A;
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void LD_A_B(void) {
	REG_A = REG_B;
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void LD_A_C(void) {
	REG_A = REG_C;
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void LD_A_D(void) {
	REG_A = REG_D;
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void LD_A_E(void) {
	REG_A = REG_E;
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void LD_A_H(void) {
	REG_A = REG_H;
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void LD_A_L(void) {
	REG_A = REG_L;
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void LD_A_BCP(void) {
	REG_A = readByte(REG_BC);
	CPU.cycles = 8;
	CPU.PC++;
}

static inline void LD_A_DEP(void) {
	REG_A = readByte(REG_DE);
	CPU.cycles = 8;
	CPU.PC++;
}

static inline void LD_A_HLP(void) {
	REG_A = readByte(REG_HL);
	CPU.cycles = 8;
	CPU.PC++;
}

static inline void LD_A_IM16P(void) {
	uint16_t im16 = read2Bytes(CPU.PC + 1);

	REG_A = readByte(im16);

	CPU.cycles = 16;
	CPU.PC += 3;
}

static inline void LD_A_IM8(void) {
	uint8_t im8 = readByte(CPU.PC + 1);

	REG_A = im8;

	CPU.cycles = 8;
	CPU.PC += 2;
}
//end of LD A,n

//LD n, A
static inline void LD_BCP_A(void) {
	writeByte(REG_BC, REG_A);
	CPU.cycles = 8;
	CPU.PC++;
}

static inline void LD_DEP_A(void) {
	writeByte(REG_DE, REG_A);
	CPU.cycles = 8;
	CPU.PC++;
}

static inline void LD_HLP_A(void) {
	writeByte(REG_HL, REG_A);
	CPU.cycles = 8;
	CPU.PC++;
}

static inline void LD_IM16P_A(void) {
	uint16_t im16 = read2Bytes(CPU.PC + 1);

	writeByte(im16, REG_A);
	CPU.cycles = 16;
	CPU.PC += 3;
}

//end of LD n,A

static inline void LD_A_FF_C(void){
	REG_A = readByte(0xFF00 + REG_C);
	CPU.cycles = 8;
	CPU.PC++;
}

static inline void LD_FF_C_A(void){
	writeByte(0xFF00 + REG_C, REG_A);
	CPU.cycles = 8;
	CPU.PC++;
}

static inline void LDI_A_HLP(void){
	REG_A = readByte(REG_HL);
	REG_HL++;
	CPU.cycles = 8;
	CPU.PC++;
}

static inline void LDD_A_HLP(void){
	REG_A = readByte(REG_HL);
	REG_HL--;
	CPU.cycles = 8;
	CPU.PC++;
}

static inline void LDI_HLP_A(void){
	writeByte(REG_HL, REG_A);
	REG_HL++;
	CPU.cycles = 8;
	CPU.PC++;
}

static inline void LDD_HLP_A(void){
	writeByte(REG_HL, REG_A);
	REG_HL--;
	CPU.cycles = 8;
	CPU.PC++;
}

static inline void LD_FF_IM8_A(void){
	uint8_t im8 = readByte(CPU.PC + 1);

	writeByte((0xFF00 + im8), REG_A);
	CPU.cycles = 12;
	CPU.PC += 2;
}

static inline void LD_A_FF_IM8(void){

	uint8_t im8 = readByte(CPU.PC + 1);

	REG_A = readByte(0xFF00 + im8);

	CPU.cycles = 12;
	CPU.PC += 2;
}

//////////////////////////
//16-BIT LOAD INSTRUCTIONS
//LD n,nn
static inline void LD_BC_IM16(void){
	uint16_t im16 = read2Bytes(CPU.PC + 1);

	REG_BC = im16;
	CPU.cycles = 12;
	CPU.PC += 3;
}

static inline void LD_DE_IM16(void){
	uint16_t im16 = read2Bytes(CPU.PC + 1);

	REG_DE = im16;
	CPU.cycles = 12;
	CPU.PC += 3;

}

static inline void LD_HL_IM16(void){
	uint16_t im16 = read2Bytes(CPU.PC + 1);

	REG_HL = im16;
	CPU.cycles = 12;
	CPU.PC += 3;
}

static inline void LD_SP_IM16(void){
	uint16_t im16 = read2Bytes(CPU.PC + 1);

	CPU.SP = im16;
	CPU.cycles = 12;
	CPU.PC += 3;
}

static inline void LD_SP_HL(void){
	CPU.SP = REG_HL;
	CPU.cycles = 8;
	CPU.PC++;
}

static inline void LD_HL_SP_IM8(void){
	int im8 = (int)((int8_t)readByte(CPU.PC + 1));
	int result = (int)CPU.SP + im8;
	REG_HL = result;

	RESET_FLAG_Z;
	RESET_FLAG_N;

	if (im8 >= 0) {
		if ((CPU.SP & 0xFF) + im8 > 0xFF)
			SET_FLAG_C;
		else
			RESET_FLAG_C;

		if ((CPU.SP & 0x0F) + (im8 & 0x0F) > 0x0F)
			SET_FLAG_H;
		else
			RESET_FLAG_H;
	}
	else		
	{
		if ((result & 0xFF) <= (CPU.SP & 0xFF))
			SET_FLAG_C;
		else
			RESET_FLAG_C;

		if ((result & 0x0F) <= (CPU.SP & 0x0F))
			SET_FLAG_H;
		else
			RESET_FLAG_H;
	}

	CPU.cycles = 12;
	CPU.PC += 2;
}

static inline void LD_IM16P_SP(void){
	uint16_t im16 = read2Bytes(CPU.PC + 1);

	write2Bytes(im16, CPU.SP);
	CPU.cycles = 20;
	CPU.PC += 3;
}

static inline void PUSH_AF(void){
	writeToStack(REG_AF);
	CPU.cycles = 16;
	CPU.PC++;
}

static inline void PUSH_BC(void){
	writeToStack(REG_BC);
	CPU.cycles = 16;
	CPU.PC++;
}

static inline void PUSH_DE(void){
	writeToStack(REG_DE);
	CPU.cycles = 16;
	CPU.PC++;
}

static inline void PUSH_HL(void){
	writeToStack(REG_HL);
	CPU.cycles = 16;
	CPU.PC++;
}

static inline void POP_AF(void){
	REG_AF = readFromStack();
	REG_F &= 0xF0;
	CPU.cycles = 12;
	CPU.PC++;
}

static inline void POP_BC(void){
	REG_BC = readFromStack();
	CPU.cycles = 12;
	CPU.PC++;
}

static inline void POP_DE(void){
	REG_DE = readFromStack();
	CPU.cycles = 12;
	CPU.PC++;
}

static inline void POP_HL(void){
	REG_HL = readFromStack();
	CPU.cycles = 12;
	CPU.PC++;
}

static inline void add(uint8_t value) {
	int prevRegA = (int)REG_A;
	int newRegA = (int)REG_A + (int)value;

	REG_A = newRegA;

	if (REG_A == 0)
		SET_FLAG_Z;
	else
		RESET_FLAG_Z;

	if (((prevRegA & 0x0F) + (value & 0x0F)) & 0x10)
		SET_FLAG_H;
	else
		RESET_FLAG_H;

	if (newRegA > 0xFF)
		SET_FLAG_C;
	else
		RESET_FLAG_C;

	RESET_FLAG_N;
}

//////////////////////////
// 8-bit ARITHMETIC
// Add A,n

static inline void ADD_A_A(void) {
	add(REG_A);
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void ADD_A_B(void) {
	add(REG_B);
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void ADD_A_C(void) {
	add(REG_C);
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void ADD_A_D(void) {
	add(REG_D);
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void ADD_A_E(void) {
	add(REG_E);
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void ADD_A_H(void) {
	add(REG_H);
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void ADD_A_L(void) {
	add(REG_L);
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void ADD_A_HLP(void) {
	add(readByte(REG_HL));
	CPU.cycles = 8;
	CPU.PC++;
}

static inline void ADD_A_IM8(void) {
	uint8_t im8 = readByte(CPU.PC + 1);

	add(im8);
	CPU.cycles = 8;
	CPU.PC += 2;
}

// ADC A,n
static inline void adc(uint8_t value) {
	int result = REG_A + value + FLAG_C;
	uint8_t prevRegA = REG_A;

	REG_A = result;

	if (REG_A == 0)
		SET_FLAG_Z;
	else
		RESET_FLAG_Z;

	RESET_FLAG_N;

	if (((prevRegA & 0x0F) + (value & 0x0F) + FLAG_C) & 0x10)
		SET_FLAG_H;
	else
		RESET_FLAG_H;

	if (result > 0xFF)
		SET_FLAG_C;
	else
		RESET_FLAG_C;
}

static inline void ADC_A_A(void) {
	adc(REG_A);
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void ADC_A_B(void) {
	adc(REG_B);
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void ADC_A_C(void) {
	adc(REG_C);
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void ADC_A_D(void) {
	adc(REG_D);
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void ADC_A_E(void) {
	adc(REG_E);
	CPU.cycles = 4;
	CPU.PC++;

}

static inline void ADC_A_H(void) {
	adc(REG_H);
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void ADC_A_L(void) {
	adc(REG_L);
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void ADC_A_HLP(void) {
	adc(readByte(REG_HL));
	CPU.cycles = 8;
	CPU.PC++;
}

static inline void ADC_A_IM8(void) {
	uint8_t im8 = readByte(CPU.PC + 1);

	adc(im8);
	CPU.cycles = 8;
	CPU.PC += 2;
}

//SUB n
static inline void sub(uint8_t value) {

	SET_FLAG_N; 

	if(value > REG_A) SET_FLAG_C;
	else RESET_FLAG_C;

	if((value & 0x0F) > (REG_A & 0x0f)) SET_FLAG_H;
	else RESET_FLAG_H;

	REG_A -= value;

	if(REG_A) RESET_FLAG_Z;
	else SET_FLAG_Z;
} 
static inline void SUB_A_A(void) {
	sub(REG_A);
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void SUB_A_B(void) {
	sub(REG_B);
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void SUB_A_C(void) {
	sub(REG_C);
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void SUB_A_D(void) {
	sub(REG_D);
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void SUB_A_E(void) {
	sub(REG_E);
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void SUB_A_H(void) {
	sub(REG_H);
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void SUB_A_L(void) {
	sub(REG_L);
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void SUB_A_HLP(void) {
	sub(readByte(REG_HL));
	CPU.cycles = 8;
	CPU.PC++;
}

static inline void SUB_A_IM8(void) {
	uint8_t im8 = readByte(CPU.PC + 1);

	sub(im8);
	CPU.cycles = 8;
	CPU.PC += 2;
}

//SBC n
static inline void sbc(uint8_t value) {
	int result = REG_A - value - FLAG_C;
	int prevRegA = REG_A;

	REG_A = result;

	if (REG_A == 0)
		SET_FLAG_Z;
	else
		RESET_FLAG_Z;

	SET_FLAG_N;

	if (((prevRegA & 0x0F) - (value & 0x0F) - FLAG_C) & 0x10)
		SET_FLAG_H;
	else
		RESET_FLAG_H;

	if (result < 0)
		SET_FLAG_C;
	else
		RESET_FLAG_C;
} 

static inline void SBC_A_A(void) {
	sbc(REG_A);
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void SBC_A_B(void) {
	sbc(REG_B);
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void SBC_A_C(void) {
	sbc(REG_C);
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void SBC_A_D(void) {
	sbc(REG_D);
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void SBC_A_E(void) {
	sbc(REG_E);
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void SBC_A_H(void) {
	sbc(REG_H);
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void SBC_A_L(void) {
	sbc(REG_L);
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void SBC_A_HLP(void) {
	sbc(readByte(REG_HL));
	CPU.cycles = 8;
	CPU.PC++;
}

static inline void SBC_A_IM8(void) {
	uint8_t im8 = readByte(CPU.PC + 1);

	sbc(im8);
	CPU.cycles = 8;
	CPU.PC += 2;
}

//AND n
static inline void and(uint8_t value) {
	REG_A &= value;

	if(REG_A) RESET_FLAG_Z;
	else SET_FLAG_Z;

	RESET_FLAG_N;
	RESET_FLAG_C;
	SET_FLAG_H;
}

static inline void AND_A_A(void) {
	and(REG_A);
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void AND_A_B(void) {
	and(REG_B);
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void AND_A_C(void) {
	and(REG_C);
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void AND_A_D(void) {
	and(REG_D);
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void AND_A_E(void) {
	and(REG_E);
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void AND_A_H(void) {
	and(REG_H);
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void AND_A_L(void) {
	and(REG_L);
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void AND_A_HLP(void) {
	and(readByte(REG_HL));
	CPU.cycles = 8;
	CPU.PC++;
}

static inline void AND_IM8(void) {
	uint8_t im8 = readByte(CPU.PC + 1);
	
	and(im8);
	CPU.cycles = 8;
	CPU.PC += 2;
}

//OR n
static inline void or(uint8_t value) {
	REG_A |= value;

	if(REG_A) RESET_FLAG_Z;
	else SET_FLAG_Z;

	RESET_FLAG_N;
	RESET_FLAG_H;
	RESET_FLAG_C;
}

static inline void OR_A_A(void) {
	or(REG_A);
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void OR_A_B(void) {
	or(REG_B);
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void OR_A_C(void) {
	or(REG_C);
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void OR_A_D(void) {
	or(REG_D);
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void OR_A_E(void) {
	or(REG_E);
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void OR_A_H(void) {
	or(REG_H);
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void OR_A_L(void) {
	or(REG_L);
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void OR_A_HLP(void) {
	or(readByte(REG_HL));
	CPU.cycles = 8;
	CPU.PC++;
}

static inline void OR_IM8(void) {
	uint8_t im8 = readByte(CPU.PC + 1);

	or(im8);
	CPU.cycles = 8;
	CPU.PC += 2;
}

//XOR n
static inline void xor(uint8_t value) {
	REG_A ^= value;

	if(REG_A) RESET_FLAG_Z;
	else SET_FLAG_Z;

	RESET_FLAG_N;
	RESET_FLAG_H;
	RESET_FLAG_C;
}

static inline void XOR_A_A(void) {
	xor(REG_A);
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void XOR_A_B(void) {
	xor(REG_B);
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void XOR_A_C(void) {
	xor(REG_C);
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void XOR_A_D(void) {
	xor(REG_D);
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void XOR_A_E(void) {
	xor(REG_E);
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void XOR_A_H(void) {
	xor(REG_H);
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void XOR_A_L(void) {
	xor(REG_L);
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void XOR_A_HLP(void) {
	xor(readByte(REG_HL));
	CPU.cycles = 8;
	CPU.PC++;
}

static inline void XOR_IM8(void) {
	uint8_t im8 = readByte(CPU.PC + 1);

	xor(im8);
	CPU.cycles = 8;
	CPU.PC += 2;
}

//CP n
static inline void cp(uint8_t value) {
	if(REG_A == value) SET_FLAG_Z;
	else RESET_FLAG_Z;

	if(value > REG_A) SET_FLAG_C;
	else RESET_FLAG_C;

	if((value & 0x0F) > (REG_A & 0x0F)) SET_FLAG_H;
	else RESET_FLAG_H;

	SET_FLAG_N;
}

static inline void CP_A_A(void) {
	cp(REG_A);
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void CP_A_B(void) {
	cp(REG_B);
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void CP_A_C(void) {
	cp(REG_C);
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void CP_A_D(void) {
	cp(REG_D);
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void CP_A_E(void) {
	cp(REG_E);
	CPU.cycles = 4;
	CPU.PC++;

}

static inline void CP_A_H(void) {
	cp(REG_H);
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void CP_A_L(void) {
	cp(REG_L);
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void CP_A_HLP(void) {
	cp(readByte(REG_HL));
	CPU.cycles = 8;
	CPU.PC++;
}

static inline void CP_IM8(void) {
	uint8_t im8 = readByte(CPU.PC + 1);

	SET_FLAG_N;

	if (REG_A == im8) SET_FLAG_Z;
	else RESET_FLAG_Z;

	if (im8 > REG_A) SET_FLAG_C;
	else RESET_FLAG_C;

	if ((im8 & 0x0F) > (REG_A & 0x0F)) SET_FLAG_H;
	else RESET_FLAG_H;

	CPU.cycles = 8;
	CPU.PC += 2;
}

//DEC n
static uint8_t dec(uint8_t value) {
	if(value & 0x0F) RESET_FLAG_H;
	else SET_FLAG_H;

	value--;

	if(value) RESET_FLAG_Z;
	else SET_FLAG_Z;

	SET_FLAG_N;

	return value;
}

static inline void DEC_A(void) {
	REG_A = dec(REG_A);
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void DEC_B(void) {
	REG_B = dec(REG_B);
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void DEC_C(void) {
	REG_C = dec(REG_C);
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void DEC_D(void) {
	REG_D = dec(REG_D);
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void DEC_E(void) {
	REG_E = dec(REG_E);
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void DEC_H(void) {
	REG_H = dec(REG_H);
	CPU.cycles = 4;
	CPU.PC++;

}

static inline void DEC_L(void) {
	REG_L = dec(REG_L);
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void DEC_HL(void) {
	REG_HL--;
	CPU.cycles = 8;
	CPU.PC++;
}

static inline void DEC_HLP(void) {
	writeByte(REG_HL, dec(readByte(REG_HL)));
	CPU.cycles = 12;
	CPU.PC++;
}

//INC n
static uint8_t inc(uint8_t value) {
	if((value & 0x0F) == 0x0F) SET_FLAG_H;
	else RESET_FLAG_H;

	value++;

	if(value) RESET_FLAG_Z;
	else SET_FLAG_Z;

	RESET_FLAG_N;

	return value;
}

static inline void INC_A(void) {
	REG_A = inc(REG_A);
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void INC_B(void) {
	REG_B = inc(REG_B);
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void INC_C(void) {
	REG_C = inc(REG_C);
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void INC_D(void) {
	REG_D = inc(REG_D);
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void INC_E(void) {
	REG_E = inc(REG_E);
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void INC_H(void) {
	REG_H = inc(REG_H);
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void INC_L(void) {
	REG_L = inc(REG_L);
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void INC_HL(void) {
	REG_HL++;
	CPU.cycles = 8;
	CPU.PC++;
}

static inline void INC_HLP(void) {
	writeByte(REG_HL, inc(readByte(REG_HL)));
	CPU.cycles = 12;
	CPU.PC++;
}



//////////////////////////
// 16-bit ARITHMETIC
// Add HL,n
static inline void add2BytesHL(uint16_t value) {
	int result = (int)REG_HL + (int)value;

	int carry = (int)REG_L + (int)(value & 0x00FF);
	if (carry > 0xFF)
		carry = 1;
	else
		carry = 0;

	if(((REG_H & 0x0F) + ((value & 0xFF00) >> 8 &0x0f) + carry ) & 0x10) SET_FLAG_H;
	else RESET_FLAG_H;

	if (result > 0xFFFF)
		SET_FLAG_C;
	else
		RESET_FLAG_C;

	REG_HL = result;

	RESET_FLAG_N;
}

static inline void ADD_HL_BC(void) {
	add2BytesHL(REG_BC);
	CPU.cycles = 8;
	CPU.PC++;
}

static inline void ADD_HL_DE(void) {
	add2BytesHL(REG_DE);
	CPU.cycles = 8;
	CPU.PC++;
}

static inline void ADD_HL_HL(void) {
	add2BytesHL(REG_HL);
	CPU.cycles = 8;
	CPU.PC++;
}

static inline void ADD_HL_SP(void) {
	add2BytesHL(CPU.SP);
	CPU.cycles = 8;
	CPU.PC++;
}

//ADD SP,n
static inline void ADD_SP_IM8(void) {
	int im8 = (int)((int8_t)readByte(CPU.PC + 1));

	int prevSP = CPU.SP;
	int result = (int)CPU.SP + im8;
	CPU.SP = result;

	RESET_FLAG_Z;
	RESET_FLAG_N;

	if (im8 >= 0) {
		if ((prevSP & 0xFF) + im8 > 0xFF)
			SET_FLAG_C;
		else
			RESET_FLAG_C;

		if ((prevSP & 0x0F) + (im8 & 0x0F) > 0x0F)
			SET_FLAG_H;
		else
			RESET_FLAG_H;
	}
	else		
	{
		if ((result & 0xFF) <= (prevSP & 0xFF))
			SET_FLAG_C;
		else
			RESET_FLAG_C;

		if ((result & 0x0F) <= (prevSP & 0x0F))
			SET_FLAG_H;
		else
			RESET_FLAG_H;
	}

	CPU.cycles = 16;
	CPU.PC += 2;
}

//INC nn
static inline void INC_BC(void) {
	REG_BC++;
	CPU.cycles = 8;
	CPU.PC++;
}

static inline void INC_DE(void) {
	REG_DE++;
	CPU.cycles = 8;
	CPU.PC++;
}

static inline void INC_SP(void) {
	CPU.SP++;
	CPU.cycles = 8;
	CPU.PC++;
}


//DEC nn
static inline void DEC_BC(void) {
	REG_BC--;
	CPU.cycles = 8;
	CPU.PC++;
}

static inline void DEC_DE(void) {
	REG_DE--;
	CPU.cycles = 8;
	CPU.PC++;
}

static inline void DEC_SP(void) {
	CPU.SP--;
	CPU.cycles = 8;
	CPU.PC++;
}


// DAA
static inline void DAA(void) {
	uint8_t value = REG_A;

	if (FLAG_N == 0) {		
		if (FLAG_C || value > 0x99) { value += 0x60; SET_FLAG_C; }
		if (FLAG_H || (value & 0x0F) > 0x09) { value += 0x06; }
	}
	else {  		
		if (FLAG_C) { value -= 0x60; }
		if (FLAG_H) { value -= 0x06; }
	}

	REG_A = value;

	if(REG_A == 0) SET_FLAG_Z;
	else RESET_FLAG_Z;

	RESET_FLAG_H;

	CPU.cycles = 4;
	CPU.PC++;
}

static inline void CPL(void) {
	REG_A = ~REG_A;

	SET_FLAG_N;
	SET_FLAG_H;

	CPU.cycles = 4;
	CPU.PC++;
}

static inline void CCF(void) {
	if(FLAG_C) RESET_FLAG_C;
	else SET_FLAG_C;

	RESET_FLAG_N;
	RESET_FLAG_H;

	CPU.cycles = 4;
	CPU.PC++;
}

static inline void SCF(void) {
	SET_FLAG_C;

	RESET_FLAG_N;
	RESET_FLAG_H;

	CPU.cycles = 4;
	CPU.PC++;
}

static inline void NOP(void) {
	CPU.cycles = 4;
	CPU.PC++;
}

static inline void HALT(void) {
	CPU.halt = 1;

	CPU.cycles = 4;
	CPU.PC++;
}

static inline void STOP(void) {
	CPU.halt = 1;

	CPU.cycles = 4;
	CPU.PC++;
}

//Interrupt
static inline void DI(void) {
	IR.masterEnable = 0;

	CPU.cycles = 4;
	CPU.PC++;
}

static inline void EI(void) {
	IR.masterEnable = 1;

	CPU.cycles = 4;
	CPU.PC++;
}

//Rotates and Shifts
static inline void RLCA(void) {
	int result = REG_A;

	REG_A = REG_A << 1;

	if (result > 0x7F) {
		REG_A |= 1;
		SET_FLAG_C;
	}
	else
		RESET_FLAG_C;

	RESET_FLAG_Z;
	RESET_FLAG_N;
	RESET_FLAG_H;

	CPU.cycles = 4;
	CPU.PC++;
}

static inline void RLA(void) {
	char prevFlagC = FLAG_C;

	if(REG_A & 0x80) SET_FLAG_C;
	else RESET_FLAG_C;

	REG_A <<= 1;

	if (prevFlagC) 
		REG_A |= 0x01;

	RESET_FLAG_N;
	RESET_FLAG_Z;
	RESET_FLAG_H;

	CPU.cycles = 4;
	CPU.PC++;
}

static inline void RRCA(void) {

	if(REG_A & 0x01) SET_FLAG_C;
	else RESET_FLAG_C;

	REG_A >>= 1;

	if(FLAG_C) REG_A |= 0x80;

	RESET_FLAG_N;
	RESET_FLAG_Z;
	RESET_FLAG_H;

	CPU.cycles = 4;
	CPU.PC++;
}

static inline void RRA(void) {
	char prevFlagC = FLAG_C;

	if(REG_A & 0x01) SET_FLAG_C;
	else RESET_FLAG_C;

	REG_A >>= 1;

	if (prevFlagC) 
		REG_A |= 0x80;

	RESET_FLAG_N;
	RESET_FLAG_Z;
	RESET_FLAG_H;

	CPU.cycles = 4;
	CPU.PC++;
}

//Jumps
static inline void JP_IM16(void) {
	uint16_t im16 = read2Bytes(CPU.PC + 1);
	CPU.PC = im16;
	CPU.cycles = 16;
}

static inline void JP_NZ_IM16(void) {
	uint16_t im16 = read2Bytes(CPU.PC + 1);

	if(FLAG_Z == 0) {
		CPU.PC = im16;
		CPU.cycles = 16;
	}
	else {
		CPU.PC += 3;
		CPU.cycles = 12;
	}
}

static inline void JP_Z_IM16(void) {
	uint16_t im16 = read2Bytes(CPU.PC + 1);

	if(FLAG_Z) {
		CPU.PC = im16;
		CPU.cycles = 16;
	}
	else {
		CPU.PC += 3;
		CPU.cycles = 12;
	}
}

static inline void JP_NC_IM16(void) {
	uint16_t im16 = read2Bytes(CPU.PC + 1);

	if(FLAG_C == 0) {
		CPU.PC = im16;
		CPU.cycles = 16;
	}
	else {
		CPU.PC += 3;
		CPU.cycles = 12;
	}
}

static inline void JP_C_IM16(void) {
	uint16_t im16 = read2Bytes(CPU.PC + 1);

	if(FLAG_C) {
		CPU.PC = im16;
		CPU.cycles = 16;
	}
	else {
		CPU.PC += 3;
		CPU.cycles = 12;
	}
}

static inline void JP_HL(void) {
	CPU.PC = REG_HL;
	CPU.cycles = 4;
}

static inline void JR_IM8(void) {
	int8_t im8 = readByte(CPU.PC + 1);

	CPU.PC += im8 + 2;
	CPU.cycles = 12;
}

static inline void JR_NZ_IM8(void) {
	int8_t im8 = readByte(CPU.PC + 1);

	if(FLAG_Z == 0)
	{
		CPU.PC += im8 + 2;
		CPU.cycles = 12;
	}
	else {
		CPU.PC += 2;
		CPU.cycles = 8;
	}
}

static inline void JR_Z_IM8(void) {
	int8_t im8 = readByte(CPU.PC + 1);

	if(FLAG_Z) {
		CPU.PC += im8 + 2;
		CPU.cycles = 12;
	}
	else {
		CPU.PC += 2;
		CPU.cycles = 8;
	}
}

static inline void JR_NC_IM8(void) {
	int8_t im8 = readByte(CPU.PC + 1);

	if(FLAG_C == 0) {
		CPU.PC += im8 + 2;
		CPU.cycles = 12;
	}
	else {
		CPU.PC += 2;
		CPU.cycles = 8;
	}
}

static inline void JR_C_IM8(void) {
	int8_t im8 = readByte(CPU.PC + 1);

	if(FLAG_C) {
		CPU.PC += im8 + 2;
		CPU.cycles = 12;
	}
	else {
		CPU.PC += 2;
		CPU.cycles = 8;
	}
}

//RST n
static inline void RST_0(void){
	writeToStack(CPU.PC + 1);
	CPU.PC = 0x00;
	CPU.cycles = 16;
}

static inline void RST_8(void){
	writeToStack(CPU.PC + 1);
	CPU.PC = 0x08;
	CPU.cycles = 16;
}

static inline void RST_10(void){
	writeToStack(CPU.PC + 1);
	CPU.PC = 0x10;
	CPU.cycles = 16;
}

static inline void RST_18(void){
	writeToStack(CPU.PC + 1);
	CPU.PC = 0x18;
	CPU.cycles = 16;
}

static inline void RST_20(void){
	writeToStack(CPU.PC + 1);
	CPU.PC = 0x20;
	CPU.cycles = 16;
}

static inline void RST_28(void){
	writeToStack(CPU.PC + 1);
	CPU.PC = 0x28;
	CPU.cycles = 16;
}

static inline void RST_30(void){
	writeToStack(CPU.PC + 1);
	CPU.PC = 0x30;
	CPU.cycles = 16;
}
static inline void RST_38(void){
	writeToStack(CPU.PC + 1);
	CPU.PC = 0x38;
	CPU.cycles = 16;
}

//Returns
static inline void RET(void) {
	CPU.PC = readFromStack();
	CPU.cycles = 16;
}

static inline void RET_NZ(void) {
	if(FLAG_Z == 0) {
		CPU.PC = readFromStack();
		CPU.cycles = 20;
	}
	else {
		CPU.PC++;
		CPU.cycles = 8;
	}
}

static inline void RET_Z(void) {
	if(FLAG_Z) {
		CPU.PC = readFromStack();
		CPU.cycles = 20;
	}
	else {
		CPU.PC++;
		CPU.cycles = 8;
	}
}

static inline void RET_NC(void) {
	if(FLAG_C == 0) {
		CPU.PC = readFromStack();
		CPU.cycles = 20;
	}
	else {
		CPU.PC++;
		CPU.cycles = 8;
	}
}

static inline void RET_C(void) {
	if(FLAG_C) {
		CPU.PC = readFromStack();
		CPU.cycles = 20;
	}
	else {
		CPU.PC++;
		CPU.cycles = 8;
	}
}

static inline void RETI(void) {
	IR.masterEnable = 1;

	CPU.PC = readFromStack();
	CPU.cycles = 16;
}

//CALLS

static inline void CALL_IM16(void) {

	writeToStack(CPU.PC + 3);
	CPU.PC = read2Bytes(CPU.PC + 1);

	CPU.cycles = 24;
}

static inline void CALL_NZ_IM16(void) {
	uint16_t im16 = read2Bytes(CPU.PC + 1);

	if(FLAG_Z == 0) {
		writeToStack(CPU.PC + 3);
		CPU.PC = im16;
		CPU.cycles = 24;
	}
	else {
		CPU.PC += 3;
		CPU.cycles = 12;
	}
}

static inline void CALL_Z_IM16(void) {
	uint16_t im16 = read2Bytes(CPU.PC + 1);

	if(FLAG_Z) {
		writeToStack(CPU.PC + 3);
		CPU.PC = im16;
		CPU.cycles = 24;
	}
	else {
		CPU.PC += 3;
		CPU.cycles = 12;
	}
}

static inline void CALL_NC_IM16(void) {
	uint16_t im16 = read2Bytes(CPU.PC + 1);

	if(FLAG_C == 0) {
		writeToStack(CPU.PC + 3);
		CPU.PC = im16;		
		CPU.cycles = 24;

	}
	else {
		CPU.PC += 3;
		CPU.cycles = 12;
	}
}

static inline void CALL_C_IM16(void) {
	uint16_t im16 = read2Bytes(CPU.PC + 1);

	if(FLAG_C) {
		writeToStack(CPU.PC + 3);
		CPU.PC = im16;
		CPU.cycles = 24;
	}
	else {
		CPU.PC += 3;
		CPU.cycles = 12;
	}
}

//CB
static inline void CB_IM8(void) {
	CPU.cycles = 8;
	cbOperation(readByte(CPU.PC + 1));

	CPU.PC += 2;
}


//UNDEFINED

static inline void UNDEF(void) {
	CPU.PC += 1;
}
