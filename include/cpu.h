#pragma once
#include <stdint.h>

#define REG_AF CPU.AF.value
#define REG_A CPU.AF.leftReg
#define REG_F CPU.AF.rightReg

#define REG_BC CPU.BC.value
#define REG_B CPU.BC.leftReg
#define REG_C CPU.BC.rightReg

#define REG_DE CPU.DE.value
#define REG_D CPU.DE.leftReg
#define REG_E CPU.DE.rightReg

#define REG_HL CPU.HL.value
#define REG_H CPU.HL.leftReg
#define REG_L CPU.HL.rightReg

#define FLAG_Z ((REG_F & (1 << 7)) != 0)
#define FLAG_N ((REG_F & (1 << 6)) != 0)
#define FLAG_H ((REG_F & (1 << 5)) != 0)
#define FLAG_C ((REG_F & (1 << 4)) != 0)

#define SET_FLAG_Z (REG_F |= (1 << 7))
#define SET_FLAG_N (REG_F |= (1 << 6))
#define SET_FLAG_H (REG_F |= (1 << 5))
#define SET_FLAG_C (REG_F |= (1 << 4))

#define RESET_FLAG_Z (REG_F &= ~(1 << 7))
#define RESET_FLAG_N (REG_F &= ~(1 << 6))
#define RESET_FLAG_H (REG_F &= ~(1 << 5))
#define RESET_FLAG_C (REG_F &= ~(1 << 4))

#define HALTED (CPU.halt != 0)

typedef union Reg{
	uint16_t value;
	struct byteReg{
		uint8_t rightReg;
		uint8_t leftReg;
	};
} Reg;

struct CPUInstance{
	// A,B,C,D,E,F Registers
	Reg AF;
	Reg BC;
	Reg DE;
	Reg HL;
	//Stack Pointer
	uint16_t SP;
	//Program Counter
	uint16_t PC;
	// Number of cycles that last instruction executed should take
	uint8_t cycles;
	uint8_t halt;
} extern CPU;


void CPUInit(void);
void CPUStep(void);
void bpoint(void);

void UNDEF(void);
void UNDEF(void);	// D3,DB,DD,EB,EC,ED,F4,FC,FD
void NOP(void);		// 00
void LD_BC_IM16(void);	// 01
void LD_BCP_A(void);	// 02
void INC_BC(void);	// 03
void INC_B(void);
void DEC_B(void);
void LD_B_IM8(void);	// 06
void RLCA(void);	// 07
void LD_IM16P_SP(void);	// 08
void ADD_HL_BC(void);	// 09
void LD_A_BCP(void);	// 0A
void DEC_BC(void);	// 0B
void INC_C(void);
void DEC_C(void);
void LD_C_IM8(void);	// 0E
void RRCA(void);	// 0F
void STOP(void);	// 10
void LD_DE_IM16(void);	// 11
void LD_DEP_A(void);	// 12
void INC_DE(void);
void INC_D(void);	// 13
void DEC_D(void);
void LD_D_IM8(void);	// 16
void RLA(void);		// 17
void JR_IM8(void);	// 18
void ADD_HL_DE(void);	// 19
void LD_A_DEP(void);	// 1A
void DEC_DE(void);	// 1B
void INC_E(void);
void DEC_E(void);
void LD_E_IM8(void);	// 1E
void RRA(void);		// 1F
void JR_NZ_IM8(void);	// 20
void LD_HL_IM16(void);	// 21
void LDI_HLP_A(void);	// 22
void INC_HL(void);	// 23
void INC_H(void);
void DEC_H(void);
void LD_H_IM8(void);	// 26
void DAA(void);		// 27
void JR_Z_IM8(void);	// 28
void ADD_HL_HL(void);	// 29
void LDI_A_HLP(void);	// 2A
void DEC_HL(void);	// 2B
void INC_L(void);
void DEC_L(void);
void LD_L_IM8(void);	// 2E
void CPL(void);		// 2F
void JR_NC_IM8(void);	// 30
void LD_SP_IM16(void);	// 31
void LDD_HLP_A(void);	// 32
void INC_SP(void);	// 33
void INC_HLP(void);	// 34
void DEC_HLP(void);	// 35
void LD_HLP_IM8(void);	// 36
void SCF(void);		// 37
void JR_C_IM8(void);	// 38
void ADD_HL_SP(void);	// 39
void LDD_A_HLP(void);	// 3A
void DEC_SP(void);	// 3B
void INC_A(void);
void DEC_A(void);
void LD_A_IM8(void);	// 3E
void CCF(void);		// 3F
void LD_B_C(void);
void LD_B_D(void);
void LD_B_E(void);
void LD_B_H(void);
void LD_B_L(void);
void LD_B_HLP(void);
void LD_B_A(void);
void LD_B_B(void);
void LD_C_B(void);
void LD_C_C(void);
void LD_C_D(void);
void LD_C_E(void);
void LD_C_H(void);
void LD_C_L(void);
void LD_C_HL(void);
void LD_C_B(void);
void LD_C_A(void);
void LD_D_B(void);
void LD_D_C(void);
void LD_D_D(void);
void LD_D_E(void);
void LD_D_H(void);
void LD_D_L(void);
void LD_D_HL(void);
void LD_D_A(void);
void LD_E_B(void);
void LD_E_C(void);
void LD_E_D(void);
void LD_E_E(void);
void LD_E_H(void);
void LD_E_L(void);
void LD_E_HL(void);
void LD_E_A(void);
void LD_H_B(void);
void LD_H_C(void);
void LD_H_D(void);
void LD_H_E(void);
void LD_H_H(void);
void LD_H_L(void);
void LD_H_HL(void);
void LD_H_A(void);
void LD_L_B(void);
void LD_L_C(void);
void LD_L_D(void);
void LD_L_E(void);
void LD_L_H(void);
void LD_L_L(void);
void LD_L_HL(void);
void LD_L_A(void);
void LD_HLP_B(void);
void LD_HLP_C(void);
void LD_HLP_D(void);
void LD_HLP_E(void);
void LD_HLP_H(void);
void LD_HLP_L(void);
void LD_HLP_A(void);
void LD_A_B(void);
void LD_A_C(void);
void LD_A_D(void);
void LD_A_E(void);
void LD_A_H(void);
void LD_A_L(void);
void LD_A_HLP(void);
void LD_A_A(void);
void ADD_A_B(void);
void ADD_A_C(void);
void ADD_A_D(void);
void ADD_A_E(void);
void ADD_A_H(void);
void ADD_A_L(void);
void ADD_A_A(void);
void ADD_A_HLP(void);
void ADC_A_A(void);
void ADC_A_B(void);
void ADC_A_C(void);
void ADC_A_D(void);
void ADC_A_E(void);
void ADC_A_H(void);
void ADC_A_L(void);
void ADC_A_HLP(void);
void ADC_A_A(void);
void SBC_A_B(void);
void SBC_A_C(void);
void SBC_A_D(void);
void SBC_A_E(void);
void SBC_A_H(void);
void SBC_A_L(void);
void SBC_A_HLP(void);
void SBC_A_A(void);
void LD_B_HLP(void);	// 46
void LD_C_HLP(void);	// 4E
void LD_D_HLP(void);	// 56
void LD_E_HLP(void);	// 5E
void LD_H_HLP(void);	// 66
void LD_L_HLP(void);	// 6E
void LD_HL_R(void);	// 70-75,77
void HALT(void);	// 76
void LD_A_HL(void);	// 7E
void SUB_A_B(void);	// 90-95,97
void SUB_A_C(void);	// 90-95,97
void SUB_A_D(void);	// 90-95,97
void SUB_A_E(void);	// 90-95,97
void SUB_A_H(void);	// 90-95,97
void SUB_A_L(void);	// 90-95,97
void SUB_A_HLP(void);	// 96
void SUB_A_A(void);	// 90-95,97
void SBC_A_A_A_R(void);	// 98-9D,9F
void SBC_A_A_A_HL(void);	// 9E
void AND_A_B(void);	// A0-A5,A7
void AND_A_C(void);	// A0-A5,A7
void AND_A_D(void);	// A0-A5,A7
void AND_A_E(void);	// A0-A5,A7
void AND_A_H(void);	// A0-A5,A7
void AND_A_L(void);	// A0-A5,A7
void AND_A_HLP(void);	// A6
void AND_A_A(void);	// A0-A5,A7
void XOR_A_B(void);	// A8-AD,AF
void XOR_A_C(void);	// A8-AD,AF
void XOR_A_D(void);	// A8-AD,AF
void XOR_A_E(void);	// A8-AD,AF
void XOR_A_H(void);	// A8-AD,AF
void XOR_A_L(void);	// A8-AD,AF
void XOR_A_HLP(void);	// AE
void XOR_A_A(void);	// A8-AD,AF
void OR_A_B(void);
void OR_A_C(void);
void OR_A_D(void);
void OR_A_E(void);
void OR_A_H(void);
void OR_A_L(void);
void OR_A_HLP(void);	// B6
void OR_A_A(void);
void CP_A_B(void);	// B8
void CP_A_C(void);	// B8
void CP_A_D(void);	// B8
void CP_A_E(void);	// B8
void CP_A_H(void);	// B8
void CP_A_L(void);	// B8
void CP_A_HLP(void);	// BE
void CP_A_A(void);	// B8
void RET_NZ(void);
void RET_Z(void);
void RET_NC(void);
void RET_C(void);
void POP_BC(void);	// C1
void JP_NZ_IM16(void);	// C2
void JP_IM16(void);	// C3
void CALL_NZ_IM16(void);	// C4
void PUSH_BC(void);	// C5
void ADD_A_IM8(void);	// C6
void RST_0(void);	// C7
void RET(void);		// C9
void JP_Z_IM16(void);	// CA
void CB_IM8(void);	// CB
void CALL_Z_IM16(void);	// CC
void CALL_IM16(void);	// CD
void ADC_A_IM8(void);	// CE
void RST_8(void);	// CF
void POP_DE(void);	// D1
void JP_NC_IM16(void);	// D2
void CALL_NC_IM16(void);	// D4
void PUSH_DE(void);	// D5
void SUB_A_IM8(void);	// D6
void RST_10(void);	// D7
void RETI(void);	// D9
void JP_C_IM16(void);	// DA
void CALL_C_IM16(void);	// DC
void SBC_A_IM8(void);	// DE
void RST_18(void);	// DF
void LD_FF_IM8_A(void);	// E0
void POP_HL(void);	// E1
void LD_FF_C_A(void);	// E2
void PUSH_HL(void);	// E5
void AND_IM8(void);	// E6
void RST_20(void);	// E7
void ADD_SP_IM8(void);	// E8
void JP_HL(void);	// E9
void LD_IM16P_A(void);	// EA
void XOR_IM8(void);	// EE
void RST_28(void);	// EF
void LD_A_FF_IM8(void);	// F0
void POP_AF(void);	// F1
void LD_A_FF_C(void);	// F2
void DI(void);		// F3
void PUSH_AF(void);	// F5
void OR_IM8(void);	// F6
void RST_30(void);	// F7
void LD_HL_SP_IM8(void);	// F8
void LD_SP_HL(void);	// F9
void LD_A_IM16P(void);	// FA
void EI(void);		// FB
void CP_IM8(void);	// FE
void RST_38(void);	// FF
