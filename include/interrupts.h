#pragma once
#include <stdint.h>

#define IR_VBLANK (1 << 0)
#define IR_LCD_STAT (1 << 1)
#define IR_TIMER (1 << 2)
#define IR_SERIAL (1 << 3)
#define IR_JOYPAD (1 << 4)

#define IR_SET_VBLANK (IR.fl |= IR_VBLANK);
#define IR_RESET_VBLANK (IR.fl &= ~IR_VBLANK);

#define IR_SET_LCD_STAT (IR.fl |= IR_LCD_STAT);
#define IR_RESET_LCD_STAT (IR.fl &= ~IR_LCD_STAT);

#define IR_SET_TIMER (IR.fl |= IR_TIMER);
#define IR_RESET_TIMER (IR.fl &= ~IR_TIMER);

#define IR_SET_SERIAL (IR.fl |= IR_SERIAL);
#define IR_RESET_SERIAL (IR.fl &= ~IR_SERIAL);

#define IR_SET_JOYPAD (IR.fl |= IR_JOYPAD);
#define IR_RESET_JOYPAD (IR.fl &= ~IR_JOYPAD);

struct Interrupt{
	uint8_t masterEnable;
	uint8_t enable;
	uint8_t fl;
} extern IR;

extern int divClocksum;	
extern int timerClocksum;

void IRInit();
void IRStep(uint32_t cycles);
void vblank(void);
void lcdStat(void);
void timer(uint32_t cycles);
void serial(void);
void joypad(void);
